package net.guides.springboot2.springboot2jpacrudexample.controller;

import java.util.Comparator;

import net.guides.springboot2.springboot2jpacrudexample.model.Employee;

public class EmployeeNameComparator implements Comparator<Employee>{

	@Override
	public int compare(Employee o1, Employee o2) {
		// TODO Auto-generated method stub
		 
		        return o1.getFirstName().concat(o1.getLastName()).compareTo(o2.getFirstName().concat(o2.getLastName())); 
		   
	}

}
